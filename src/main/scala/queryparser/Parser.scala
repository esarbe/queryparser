package queryparser

import scala.annotation.tailrec
import cats.Semigroupal
import cats.syntax.all.*
import cats.Functor
import cats.data.NonEmptyList
import cats.data.ValidatedNel
import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import scala.collection.immutable.ListSet

sealed trait Query
object Query {
  case class Term(value: String) extends Query
  case class Argument(key: String, value: String) extends Query
  sealed trait Validation
  object Validation {
    case class IllegalValue(message: String) extends Validation
    case class SyntaxError(message: String) extends Validation
  }
}

sealed trait Match[+A] {
  def map[B](f: A => B): Match[B]
}

object Match {

  case class Consumed[A](next: A) extends Match[A] {
    def map[B](f: A => B): Match[B] = Consumed(f(next))
  }

  case class Rejected[A](next: A, reason: NonEmptyList[Error]) extends Match[A] {
    def map[B](f: A => B): Match[B] = Rejected(f(next), reason)
  }
}

trait Accumulator[A] {
  def parseQuery(query: Query): Option[Match[Accumulator[A]]]
  def result: Accumulator.Result[A]
  def description: String 
  def map[B](f: A => B): Accumulator[B] = Accumulator.Map(this, f)
}


object Accumulator {
  type Result[A] = cats.data.ValidatedNel[Error, A]

  implicit val accumulatorFunctor: Functor[Accumulator] = new Functor[Accumulator] {
    override def map[A, B](fa: Accumulator[A])(f: A => B): Accumulator[B] = fa.map(f)
  }

  case class Map[A, B](acc: Accumulator[A], f: A => B) extends Accumulator[B] {

    override def parseQuery(query: Query): Option[Match[Accumulator[B]]] = 
      acc.parseQuery(query).map(_.map { copy(_, f) })

    override def description = acc.description
    override def result: Result[B] = acc.result.map(f)
  }


  case class Term(value: Option[String]) extends Accumulator[String] {

    override def parseQuery(query: Query): Option[Match[Accumulator[String]]] = 
      Option(query).collect {
        case Query.Term(term) if this.value.isEmpty =>
           Match.Consumed(copy(value = Some(term)))
      }
      
    override def description = "<string>"  
    override def result: Result[String] = {
      value.toValidNel(Error.MissingTerm)
    }
  }


  case class Argument(key: String, value: Option[String]) extends Accumulator[String] {

    override def parseQuery(query: Query): Option[Match[Argument]] = 
      Option(query)
        .collect { case Query.Argument(key, value) if this.key == key && this.value.isEmpty =>
          Match.Consumed(copy(value = Some(value)))
        }
    
    override def description = s"$key:<string>"
    override def result: Result[String] = value.toValidNel(Error.MissingArgument(key))
  }

  case class KeyedArgument[T](
    argument: Argument,
    label: String,
    typeName: String,
    exampleValue: String,
    parse: String => ValidatedNel[Query.Validation, T],
  ) extends Accumulator[T] {
    override def parseQuery(query: Query) = 
      argument.parseQuery(query).map(_.map(copy(_)))

    override def description = 
      s"""$label
         |\t ${argument.key}:<$typeName>
         |\t Example: `${argument.key}:$exampleValue` 
         |""".stripMargin

    override def result = 
      argument.result.andThen(a => parse(a).leftMap(_.map(Error.ValidationError(argument.key, _))))
  }


  /**
   * the root accumulator will return an error if it cannot handle the query
   */
  case class Root[A](acc: Accumulator[A]) extends Accumulator[A] {
    override def parseQuery(query: Query): Option[Match[Accumulator[A]]] = {
      acc.parseQuery(query).orElse(Some(Match.Rejected(this, NonEmptyList.of(Error.unexpected(query)))))
    }
    def description = acc.description
    def result: Result[A] = acc.result
  }

  case class And[A, B](left: Accumulator[A], right: Accumulator[B]) extends Accumulator[(A, B)] {

    def parseQuery(query: Query): Option[Match[Accumulator[(A, B)]]] = 
      (left.parseQuery(query), right.parseQuery(query)) match {
        case (Some(Match.Consumed(nextLeft)), Some(Match.Consumed(nextRight))) => 
          Some(Match.Consumed(And(nextLeft, nextRight)))
        case (Some(Match.Consumed(nextLeft)), None) =>
          Some(Match.Consumed(And(nextLeft, right)))
        case (None, Some(Match.Consumed(nextRight))) => 
          Some(Match.Consumed(And(left, nextRight)))
        case _ => None
      }
    
    override def description = 
      s"""|${left.description} 
          |AND      
          |${right.description}"""
            .stripMargin

    override def result: Result[(A, B)] = {
      (left.result,  right.result).mapN((_, _))
    }
  }

  case class Many[A](acc: Accumulator[A], as: List[Accumulator[A]]) extends Accumulator[Seq[A]] {
    override def parseQuery(query: Query): Option[Match[Accumulator[Seq[A]]]] = {
      acc.parseQuery(query).collect {
        case Match.Consumed(next) => Match.Consumed(copy(as = next :: as))
      }
    }

    override def description: String = s"[ ${acc.description}, ... ]"

    override def result: Result[Seq[A]] = {
      as.traverse(_.result).map(_.reverse)
    }
  }

  case class Optional[A](acc: Accumulator[A]) extends Accumulator[Option[A]] {
    override def parseQuery(query: Query): Option[Match[Accumulator[Option[A]]]] = 
      acc.parseQuery(query).collect {
        case Match.Consumed(next) => Match.Consumed(copy(acc = next))
      }

    override def description = s"[${acc.description}]"
    override def result: Result[Option[A]] = 
      acc.result.map(Some.apply).orElse(Valid(None))
  }


  implicit val argumentSemiGroupal: Semigroupal[Accumulator] = new Semigroupal[Accumulator]{
    override def product[A, B](fa: Accumulator[A], fb: Accumulator[B]): Accumulator[(A, B)] = 
      Accumulator.And(fa, fb)
  }

  extension [A](acc: Accumulator[A]) {
    def and[B](other: Accumulator[B]) = And(acc, other)
    def parse(args: List[String]): Result[A] = Parser.consumeAll(args, acc)
  }

  def term[A]: Term = Term(None)
  def arg(key: String): Argument = Argument(key, None)
  def optional[A](acc: Accumulator[A]): Accumulator[Option[A]] = Optional(acc)
  def many[A](acc: Accumulator[A]): Accumulator[Seq[A]] = Many(acc, Nil)
}

sealed trait Error
object Error {
  case class UnexpectedArgument(key: String, value: String) extends Error
  case class UnexpectedTerm(value: String) extends Error
  case object MissingTerm extends Error
  case class MissingArgument(key: String) extends Error
  case class ValidationError(key: String, reason: Query.Validation) extends Error

  def unexpected(query: Query): Error = query match {
    case Query.Argument(key, value) => UnexpectedArgument(key, value)
    case Query.Term(term) => UnexpectedTerm(term)
  }
}

object Parser extends App {
  val IsArgument = "(.+?):(.+?)".r

  case class Key[T](
    label: String,
    key: String,
    typeName: String,
    parse: String => ValidatedNel[Query.Validation, T],
    exampleValue: String,
    occurrences: Key.Occurrences,
  )

  object Key {
    trait Occurrences
    object Occurrences {
      case object Unique extends Occurrences //exactly one
      case object AtLeastOne extends Occurrences // 1 and more
      case object Multiple extends Occurrences // 0 and more
      case object Optional extends Occurrences // 0 or 1 
    }
  }

  @tailrec
  def consumeAll[A](args: List[String], acc: Accumulator[A]): Validated[NonEmptyList[Error], A] = {

    args match {
      case IsArgument(key, value) :: rest =>
        acc.parseQuery(Query.Argument(key, value)) match {
          case Some(Match.Consumed(next)) => consumeAll(rest, next)
          case Some(Match.Rejected(next, reason)) => consumeAll(rest, next)
          case None => Invalid(Error.UnexpectedArgument(key, value)).toValidatedNel *> acc.result
        }
      case first :: rest => 
        acc.parseQuery(Query.Term(first)) match {
          case Some(Match.Consumed(next)) => consumeAll(rest, next)
          case Some(Match.Rejected(next,reason)) => consumeAll(rest, next)
          case None => Invalid(Error.UnexpectedTerm(first)).toValidatedNel *> acc.result
        }

      case Nil => acc.result
    }
  }

  def argFromKey[T](key: Key[T]) = {
    val argument = Accumulator.arg(key.key)
    val keyedArgument = Accumulator.KeyedArgument(argument, key.label, key.typeName, key.exampleValue, key.parse)
    keyedArgument
  }

  def parse[A](args: List[String], acc: Accumulator[A]) = {
    consumeAll(args, Accumulator.Root(acc))
  }  
}
