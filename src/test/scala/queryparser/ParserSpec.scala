package queryparser

import Accumulator.*
import ParserSpec.*

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import cats.data.NonEmptyList
import cats.data.Validated.{Invalid, Valid}
import cats.data.ValidatedNel
import cats.syntax.all.*

class ParserSpec extends AnyFlatSpec with Matchers {
  import Accumulator.* 

  behavior of "Argument"
  it should "accept a query argument of the same key and keep its value" in {
    val input = List("foo:bar")
    val acc = Argument("foo", None)
    Parser.consumeAll(input, acc) shouldBe Valid("bar")
  }
  
  it should "accept only exactly one argument of the same key and raise an error for any further" in {
    val input = List("foo:bar", "foo:qux")
    val acc = Argument("foo", None)
    Parser.consumeAll(input, acc) shouldBe Invalid(Error.UnexpectedArgument("foo", "qux")).toValidatedNel
  }


  it should "accept only exactly one term and raise an error for any further" in {
    val input = List("foo", "bar")
    val acc = Term(None)
    Parser.consumeAll(input, acc) shouldBe Invalid(Error.UnexpectedTerm("bar")).toValidatedNel
  }

  it should "complain about a missing key for an empty input" in {
    val input = List()
    val acc = Argument("key", None)

    Parser.consumeAll(input, acc) shouldBe Invalid(Error.MissingArgument("key")).toValidatedNel
  }


  it should "complain about a missing key for only a term" in {

    val input = List("term")
    val acc = Argument("key", None)

    Parser.consumeAll(input, acc) shouldBe 
      Invalid(NonEmptyList.of(Error.UnexpectedTerm("term"), Error.MissingArgument("key")))
  }

  behavior of "Term"
  it should "complain about a missing term for an empty input" in {
    val input = List()
    val acc = Term(None)

    Parser.consumeAll(input, acc) shouldBe Invalid(Error.MissingTerm).toValidatedNel
  }

  it should "complain about a missing term for only an argument" in {
    val input = List("foo:bar")
    val acc = Term(None)

    Parser.consumeAll(input, acc) shouldBe 
      Invalid(NonEmptyList.of(Error.UnexpectedArgument("foo", "bar"), Error.MissingTerm))
  }

  behavior of "And"
  it should "receive both sides" in {
    val acc = And(Term(None), Argument("foo", None))
    val input = List("foo:bar", "qux")
    Parser.consumeAll(input, acc) shouldBe Valid(("qux", "bar"))
  }

  it should "return an error if one side fails" in {
    val acc = And(Term(None), Argument("foo", None))
    val inputArgumentOnly = List("foo:bar")
    Parser.consumeAll(inputArgumentOnly, acc) shouldBe Invalid(Error.MissingTerm).toValidatedNel
    val inputTermOnly = List("qux")
    Parser.consumeAll(inputTermOnly, acc) shouldBe Invalid(Error.MissingArgument("foo")).toValidatedNel
  }

  it should "return an error if both sides fails" in {
    val acc = And(Term(None), Argument("foo", None))
    val inputTermOnly = List()  

    Parser.consumeAll(inputTermOnly, acc) shouldBe 
      Invalid(NonEmptyList(Error.MissingTerm, List(Error.MissingArgument("foo"))))
  }

 
  behavior of "Many"
  it should "receive multiple entries for an argument" in {
    val acc = Many(Term(None), Nil)
    val input = List("foo", "bar", "qux")
    Parser.consumeAll(input, acc) shouldBe Valid(List("foo", "bar", "qux"))
  }

  behavior of "Optional"
  it should "take at least none" in {
    val acc = Optional(Term(None))
    val input = List()
    Parser.consumeAll(input, acc) shouldBe Valid(None)
  }

  behavior of "Complex query"
  it should "accept a complex query" in  {
      
    val input = List("foo", "bar", "qux", "client:1.2.3.4", "user:fiver", "-user:hazel")

    queryParser.parse(input) shouldBe 
      Valid(Parsed(Ip("1.2.3.4"), User("fiver"), List(Excluded(User("hazel"))), List(Criteria("foo"), Criteria("bar"), Criteria("qux"))))

    queryParser.parse(List()) shouldBe
      Invalid(NonEmptyList.of(Error.MissingArgument("client"), Error.MissingArgument("user")))  
  }
 
  it should "give a description" in {


    Parser.argFromKey(clientKey).description shouldBe 
      """Client
        |	 client:<ip>
        |	 Example: `client:1.2.3.4` 
        |""".stripMargin

    queryParser.description shouldBe 
      """Client
        |	 client:<ip>
        |	 Example: `client:1.2.3.4` 
        | 
        |AND      
        |user:<string> 
        |AND      
        |[ -user:<string>, ... ] 
        |AND      
        |[ <string>, ... ]"""
          .stripMargin
  }

}

object ParserSpec {

  val clientKey = 
    Parser.Key(
      "Client",
      "client",
      "ip",
      Ip.parse(_),
      "1.2.3.4",
      Parser.Key.Occurrences.Unique,
    )
  

  case class Ip(value: String)
  object Ip {
    def parse(value: String): ValidatedNel[Query.Validation, Ip] = {
      val result =
        if (value.startsWith("1")) Ip(value).valid
        else Query.Validation.SyntaxError("must start with 1").invalid

      result.toValidatedNel
    }
  }


  case class Criteria(value: String)
  object Criteria {
    def parse(value: String): ValidatedNel[Query.Validation, Criteria] = Criteria(value).valid.toValidatedNel
  }

  case class Excluded[A](a: A)

  case class User(value: String)
  object User {
    def parse(value: String) = User(value).valid.toValidatedNel
  }

  case class Parsed(ip: Ip, user: User, excludedUsers: Seq[Excluded[User]], terms: Seq[Criteria])

  val queryParser = 
    (
      Parser.argFromKey(clientKey),
      arg("user").map(User.apply),
      many(arg("-user").map(u => Excluded(User(u)))),
      many(term.map(term => Criteria(term))),
    ).mapN { case (client, user, excludedUsers, terms) =>
      Parsed(client, user, excludedUsers, terms.toSeq)
    }

}