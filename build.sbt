name := "queryparser"

scalaVersion := "3.4.0"

libraryDependencies += "org.typelevel" %% "cats-core" % "2.10.0"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.18" 
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.18" % "test"
